import React from 'react';
import Loader from 'react-loader-spinner';
import "./LoadingOverlay.css"; //오버레이 css파일 끼워씁시다

const LoadingOverlay = (props) => {
    return (
    <div className="loading-overlay">
        <Loader type={"BallTriangle"} color="#008800" width={30} height={30} />
    </div>
    );
};

export default LoadingOverlay;