import React, { useEffect, useState } from "react";
import axios from "axios";
import moment from 'moment';
import "moment/locale/ko"
import Moment from 'react-moment';
import LoadingOverlay from './components/LoadingOverlay/LoadingOverlay'; //로딩오버레이 참조
import "./App.css";

//jy_darkmode
import useDarkMode from 'use-dark-mode';
import Switch from "react-switch";
//jy_darkmode

moment.locale('ko'); //moment모듈 한글패치적용

function App() {  
  const [loading, setLoading] = useState(true);
  const [todolist, setTodolist] = useState({ items: [] });
  const [open, toggleOpen] = useState(false);
  const [todoCount, setTodoCount] = useState(0);

  // jy_darkmode -다크모드 버튼 && 스위치적용
  const darkMode = useDarkMode(false); //다크모드 버튼
  const [checked, setChecked] = useState(false); //스위치
  
  // <button type="button" onClick={darkMode.disable}>☀</button>
  // <button type="button" onClick={darkMode.enable}> ☾</button>

  const handleChange = nextChecked => {
    setChecked(nextChecked);
    if (nextChecked === true) {
      darkMode.enable();      
    } else {
      darkMode.disable();
      };
  };
  // jy_darkmodes

  // BTP용
  const host = "https://node-todolist-jjy4378.cfapps.us10.hana.ondemand.com";
  // 업데이트용
  // const host = "http://localhost:5000";
  // 모바일용
  // const host = "https://97c3c3ca5dbd.ngrok.io";

   //미체크인것만 카운트
   const count = (items) => {
    let i = 0;
    items.map((item) => {
      (!item.todo_done) && (i++)
    });
    return i;
  }

  //DB 연결한 Nodejs서버에 유저 정보를 가져와 options에 연결
  const getTodolist = () => {
    const options = {
      url : `${host}/api/v1/todolist`,
      method: "GET",
      headers: {
        user_id : "2574151",
      },
    };

    //받아온 정보 getTodolist 리턴
    return axios(options);
  };

  // DB 업데이트를 위한 비동기 통신
  // Content-Type 은 형식적으로 적어주는 구문/없어도 작동은 한다
  const updateTodolist = (todolist) => {
    const options = {
    url : `${host}/api/v1/todolist`,
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
    },
    //데이터를 받아서 전달
    data: todolist,
    };

    return axios(options);
};

  const sendMessage = (user_id) => {
    const options = {
      url: `${host}/api/v1/todolist/message`,
      method: "POST",
      data: {
        user_id : user_id,
      }
    };
    return axios(options);
  };

  // e.target 에 어떤걸 눌러서 이벤트에 왔는지 알수있다
  const handleOpen = (e) => {
    toggleOpen(!open);
    console.log(open);
  };

  // e.key 에 어떤걸 눌러서 이벤트에 왔는지 알수있다
  const handleKeyPress = (e) => {
    // alert 은 알람
    if (e.key === "Enter") {
      // 엔터눌렀을떄 들어오는 값
      const todoText = e.target.value;
      // 원본 todolist 카피
      const copydata = {...todolist}
      // 새로운 아이템 생성
      const item = {
        "id": "2",
        "createdAt": moment().format("YYYY-MM-DD HH:mm:ss"),
        "modifiedAt": moment().format("YYYY-MM-DD HH:mm:ss"),
        "todo_title": todoText,
        "todo_done": false,
      };

      setLoading(true);

      // user_id로 url통신
      sendMessage(copydata.user_id).then((response) => {
      // 배열의 마지막요소에 하나 추가
      copydata.items.push(item);

      updateTodolist(copydata).then((response) => {
        if (response.data) {
          setTodolist(response.data);
          setTodoCount(count(response.data.items));
          // setTodoCount(response.data.items.length);
          // 완료 후 타켓을 빈값으로
          e.target.value = "";
          //추가 완료 후 시트 닫기
          toggleOpen(!open);
          setLoading(false);
      
        }
        }) .catch((error) => {
          setLoading(false);
          debugger;
          });
          }) .catch ((error) => {
            setLoading(false);
            debugger;
            });
      };
  };

  const handleDone = (e) => {
    // e.target.id의 -를 뺴고 뒤집어서 첫번째 부터 가져오면 돼
    // current는 자식에게 이벤트 적용되는것을 막는다
    const id = e.currentTarget.id.split('-').reverse()[0];
    //todolist.item의 id값의todo_done의 값을 가져와서 반대로 바꾼다
    // 근데 직접변경은안돼 상태값변경함수를 통해서 변경해야해
    // const copydata = todolist;  //shallow copy 방법
    // const copydata = JSON.parse(JSON.stringify(todolist)); //deep copy (ES5)

    const copydata = {...todolist}; //deep copy (ES6)

    setLoading(true);

    todolist.items[id].todo_done = !copydata.items[id].todo_done;
    todolist.items[id].modifiedAt = moment().format("YYYY-MM-DD HH:mm:ss");

    //가져온데이터로 db업데이트작업 - 비동기동작..으로 then처리
    updateTodolist(copydata).then((response) => {
      if (response.data) setTodolist(response.data);
      //할일수량 반영을 위한 추가 - 21.04.06 JJY
      setTodoCount(count(response.data.items));
      setLoading(false);
    }).catch((error) => {
      setLoading(false);
    });

    // 변경된값을 버츄어돔에 알려준다 
    setTodolist(copydata);
  };

  const handleDelete = (e) => {
    const id = e.currentTarget.id.split('-').reverse()[0];
    const copydata = {...todolist};

    copydata.items.splice(id, 1);

    setLoading(true);

    updateTodolist(copydata).then((response) => {
      if (response.data) {
        setTodolist(response.data);
        setTodoCount(count(response.data.items));
        // setTodoCount(response.data.items.length);
        setLoading(false);
        }
    }) .catch((error) => {
      setLoading(false);
      debugger;
    });

    // const Array = [];
    //배열의 가장 마지막을 없애주는 옵션
    // Array.pop();
    //시작점으로부터 몇개를 없앨건지 지정
    // Array.splice(startIdx, range);
  };
    
  const handleCloseApp = (e) => {
    window.location.href = 'app://close'; //앱닫기 카카오워크에서만 사용가능
  };

  // 데이터 가져오기
  useEffect(() => {
    //로딩상태 보여주고
    setLoading(true);

    //최초적용시 무조건 하얀화면으로 할거야
    if (checked === false) {
      darkMode.disable();    
    }
    
    getTodolist().then( response => {
      if (response.data) {
        setTodolist(response.data);
        setTodoCount(count(response.data.items));
        // setTodoCount(response.data.items.length);
        }
        // 데이터 다 가져오면 끄기
        setLoading(false);
    })
    .catch( error => {
      setLoading(false);
      debugger;
    });
  }, []);

  return (
    <div className="App">
      <div className="app-close" onClick={handleCloseApp}>
        <div className="fas fa-times"></div>
      </div>
      {/* 로딩표시 걸어주기 */}
      {loading && <LoadingOverlay />}
      <div className="todolist-date">
        <Moment format="YYYY년 MM월 DD일">{new Date()}</Moment>
      </div>     
      <div className="todolist-dayofweek">
        <Moment format="dddd">{new Date()}</Moment>
      </div>
      <ul className="todolist-body">
      <div className="todolist-overview">{`할일 ${todoCount}개 남음`}</div>
      <div className="todolist-toggle">
      <Switch className="react-switch" onChange={handleChange} checked={checked}/>
      </div>
      </ul>
      <div className="todolist-items">
        {todolist.items.map((item, itemIdx) => {
          return (
            <div className="todolist-item" key={itemIdx}>
              <div className="todolist-item-content">      
                {/* <div className="item-check item-done"> */}
                {/*  item-itemid인덱스를 가져와 그리고 item.todo_done이 참이면 체크와던적용 아니면 체크만적용 */}
                {/* 부모에 지정된 handledone의 이벤트가 자식에게도 적용되어버림 */}
                <div id={`item-${itemIdx}`} onClick={handleDone} className={item.todo_done ? "item-check item-done" : "item-check"}>
                  {/* todo_done이 true면 진행 */}
                  {item.todo_done && <i className="fas fa-check"></i>}
                </div>
                 <div className={item.todo_done ? "item-text text-done" : "item-text"}>{item.todo_title}</div>
                 <div id={`item-${itemIdx}`} className="item-delete" onClick={handleDelete}>
                   <i className="fas fa-trash-alt"></i></div>
              </div>
                <div className="todolist-item-datetime">{item.modifiedAt}</div>
            </div>
          );
        })}
      </div>

      {open && (
              <div className="bottom">
              <div className="bottom-sheet">
              {/* 인풋에 값이없을때 보여줄 말 적기 */}
              <input 
                placeholder="입력 후, Enter를 입력하세요"
                onKeyPress={handleKeyPress}
              />
            </div>      
            </div>      
      )}

      <div className="circle-button" onClick={handleOpen}>
        <i className="fas fa-plus"></i>
      </div>    
    </div>
  );
}

export default App;
